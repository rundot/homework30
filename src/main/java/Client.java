import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by emaksimovich on 28.01.17.
 */
public class Client extends Thread {

    private final String name;

    private final static String SERVER_ADDR = "localhost";
    private final static int SERVER_PORT = 9704;
    private static Socket serverSocket;
    private static BufferedReader reader;
    private static PrintWriter writer;

    public Client(String name) {
        this.name = name;
        try {
            serverSocket = new Socket(SERVER_ADDR, SERVER_PORT);
            try {
                reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                writer = new PrintWriter(serverSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            new ServerInputHandlerThread();
            new ConsoleInputHandlerThread();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static class ServerInputHandlerThread extends Thread {

        public ServerInputHandlerThread() {
            start();
        }

        @Override
        public void run() {
            String str;
            try {
                while ((str = reader.readLine()) != null) {
                    System.out.println(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ConsoleInputHandlerThread extends Thread {

        public ConsoleInputHandlerThread() {
            this.start();
        }

        @Override
        public void run() {
            String str;
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                while ((str = reader.readLine()) != null) {
                    writer.println(name + ":" + str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Client gennadiy = new Client("Gennadiy");
    }

}
