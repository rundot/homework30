import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emaksimovich on 27.01.17.
 */
public class Server {

    private final static int PORT_NUMBER = 9704;

    private static List<ClientHandlerThread> clientHandlerThreadList = new ArrayList<>();

    public static void broadcastMessage(String message, ClientHandlerThread initializer) {
        clientHandlerThreadList
                .stream()
                .forEach(clientHandlerThread -> {
                    if (clientHandlerThread!= initializer)
                        clientHandlerThread.sendMessage(message);
                });


    }

    public static class SocketListenerThread extends Thread {

        ServerSocket serverSocket;

        public SocketListenerThread(ServerSocket serverSocket) {
            this.serverSocket = serverSocket;
            start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    clientHandlerThreadList.add(new ClientHandlerThread(serverSocket.accept()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class ClientHandlerThread extends Thread {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;

        public void sendMessage(String message) {
            writer.println(message);
        }

        public ClientHandlerThread(Socket socket) {
            this.clientSocket = socket;
            try {
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                writer = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.start();
            System.out.println("started listening to new client");
        }

        @Override
        public void run() {
            String message;
            try {
                while ((message = reader.readLine()) != null) {
                    broadcastMessage(message, this);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
        System.out.println("Server started");
        new SocketListenerThread(serverSocket);
    }

}
